<?php
//!  Vraag_model haalt vragen op.
class Deelnemer_model extends CI_Model {

    //----------------------------------------------------------------------
    //Opdracht: Project traningssessies
    //Opdrachtgever: Dirk De Peuter
    //----------------------------------------------------------------------
    //Thomas More Kempen
    //----------------------------------------------------------------------
    //auteur: Project34
    //----------------------------------------------------------------------


    function __construct() {
        parent::__construct();
    }

  
    function getAll() {
        $this->db->order_by('id', 'asc');
        $query = $this->db->get('deelnemer');
        return $query->result();
    }

}